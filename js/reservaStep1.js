(function() {
  console.log("passo1");
  if(localStorage.reservation) {
    delete localStorage.reservation;
  }


  var validateStep1 = function(e) {
    e.preventDefault();
    var totalHospedes = 0;

    if($('#dataEntrada').val().length === 0) {
      $.notify("Informe a data de Entrada / Check-in", "error");
      $('#dataEntrada').focus();
      return false;
    }

    if($('#dataSaida').val().length === 0) {
      $.notify("Informe a data de Saída / Check-out", "error");
      $('#dataSaida').focus();
      return false;
    }

    if(Number($('.numQuartos option:selected').val()) === 0) {
      $.notify("Informe a quantidade de quartos", "error");
      $('.numQuartos').focus();
      return false;
    }

    $('.qtdHospedes option:selected').each(function() {
      totalHospedes += Number($(this).val());
    });

    if(totalHospedes === 0) {
      $.notify("Informe a quantidade de hospedes", "error");
      $('.qtdHospedes:first').focus();
      return false;
    }

    if(localStorage && !localStorage.reservation) {
      localStorage.reservation = JSON.stringify({
        dataEntrada: $('#dataEntrada').val(),
        dataSaida: $('#dataSaida').val(),
        qtdQuartos: Number($('.numQuartos option:selected').val()),
        adultos: Number($('#adultosQuarto option:selected').val()),
        criancasF1: Number($('#criancaFaixa1 option:selected').val()),
        criancasF2: Number($('#criancaFaixa2 option:selected').val()),
        diasHospedagem: 0,
        total: 0,
        status: "disabled"
      });

      $('form:first').submit();
    }

  }
  $('.continueStep1')
    .on('click', validateStep1);
})();