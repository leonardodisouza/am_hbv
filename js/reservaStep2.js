(function() {
  console.log("passo2");
  var data = null;

  var init = function() {
    if(localStorage && localStorage.reservation) {
      data = JSON.parse(localStorage.reservation);
      var source = $("#infoReservation").html();
      var template = Handlebars.compile(source);
      var html = template(data);
      $(html).appendTo('.step');
    }
  }();

  var addRoom = function() {
    var data = JSON.parse(localStorage.reservation);

    if(!$(this).hasClass('choiced') && $('.choiced').length < data.qtdQuartos) {
      $(this).text("Quarto Escolhido").addClass("choiced");
        
      if(data.prices) {
        data.prices.push($(this).data('price'));
      } else {
        data.prices = [$(this).data('price')];
      }

      if(data.prices.length > data.qtdQuartos) {
        data.prices.splice(data.qtdQuartos, data.prices.length);
      }

      var totalPrices = 0;
      data.prices.map(function(price) {
        totalPrices += price;
      });

      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(Number(data.dataEntrada.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$1")), Number(data.dataEntrada.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$2")), Number(data.dataEntrada.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$3")));
      var secondDate = new Date(Number(data.dataSaida.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$1")), Number(data.dataSaida.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$2")), Number(data.dataSaida.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$3")));
      var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

      data.total = totalPrices * diffDays * data.adultos;
      data.diasHospedagem = diffDays;
      data.status = "";

      localStorage.reservation = JSON.stringify(data);

      var source = $("#infoReservation").html();
      var template = Handlebars.compile(source);
      var html = template(data);
      $(html).appendTo('.step');
    } else {
      $.notify("A quantidade de quartos indicada já foi definida", "info");
    }

    console.log(JSON.parse(localStorage.reservation));
  };

  var nextStep = function() {
    $('form:first').submit();
  };

  $('.choice-room')
    .on('click', addRoom);

  $('body')
    .on('click', '.continue', nextStep);



})();