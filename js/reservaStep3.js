(function() {
  var saveLogin = function() {
    if(localStorage && localStorage.reservation) {
      var data = JSON.parse(localStorage.reservation);
      data.email = $('[type=email]').val();
      localStorage.reservation = JSON.stringify(data);
      $('form:first').submit();
    }
  };

  $('.login')
    .on('click', saveLogin);
})();